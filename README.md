# ArduinoLed G2

El proyecto consiste en la implementación de un semáforo en Arduino. 
Se usará un Arduino Uno junto con 3 LEDS: Rojo, Amarillo y Verde. 
Además contará con un botón para ir alternando entre cada color.
Adiccionalmente se le ha incluido un pequeño buzzer que sonará cuando el semáforo se encuentre en verde.

## Autores y tareas

- Jesus Jimenez: Código de Arduino
- Javier Iglesias: Esquemáticos
- Anibal Herrera: Revisor

## Cómo usar y software a emplear

Se debe compilar el código mediante Arduino IDE y cargarlo en el hardware Arduino deseado.
Mediante el botón puedes ir alternando entre los distintos LEDS.

## Licencia

GPL  (Licencia Pública General de GNU)