//Defincion de puertos digitales para leds

int ledrojo = 2;
int ledamarillo = 3;
int ledverde = 4;
int pinBoton = 7;
int estadoBoton = LOW;
int estadoBoton02 = LOW;
int estadoBoton03 = LOW;
int estadoBoton2 = LOW;
int estadoled = 0;
int pinzumbador = 13;
int frecuencia = 220;


void setup() {
  pinMode(ledrojo , OUTPUT); 
  pinMode(ledamarillo , OUTPUT); 
  pinMode(ledverde , OUTPUT); 
  pinMode(pinBoton, INPUT);
  Serial.begin(9600);

}

void loop() {
  // Se leerá un switch digital que cambiará el led encendido en cada 
  // Evitar rebotes
  estadoBoton02 = digitalRead(pinBoton);
  delay(20);
  estadoBoton03 = digitalRead(pinBoton);
  delay(20);
  estadoBoton = digitalRead(pinBoton);
  delay(20);

  if(estadoBoton == estadoBoton02 == estadoBoton03){
  if((estadoBoton == HIGH) && (estadoBoton != estadoBoton2)){
    if(estadoled == 2){
      estadoled = 0;
    }else{
      estadoled ++;
    }
    delay(250);
  
  

    if(estadoled == 0){
      digitalWrite(ledrojo, HIGH);
      digitalWrite(ledamarillo, LOW);
      digitalWrite(ledverde, LOW);
      Serial.println("LED0");
    
    }else if(estadoled == 1){
      digitalWrite(ledrojo, LOW);
      digitalWrite(ledamarillo, HIGH);
      digitalWrite(ledverde, LOW);
      Serial.println("LED1");
    
    
    }else if(estadoled == 2){
      digitalWrite(ledrojo, LOW);
      digitalWrite(ledamarillo, LOW);
      digitalWrite(ledverde, HIGH);
      Serial.println("LED2");
      tone(pinzumbador,frecuencia);

    }
    
  }
  }
  estadoBoton2 = estadoBoton;
}
